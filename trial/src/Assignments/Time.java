package Assignments;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Time {
	public static void main(String[] args) throws Exception
    {
        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
    //    Scanner sc = new Scanner(System.in);
        Date date = parseFormat.parse("11:30 PM");
        System.out.println("Converting 12 hr clock to 24 hr clock");
        System.out.println(parseFormat.format(date)+ " = " +displayFormat.format(date));
    }
	

}
