package Assignments;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Timeconversion {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	        Scanner sc= new Scanner(System.in);
	        String s =sc.nextLine();
	        System.out.println("enter the 12 hours format time");
	        DateFormat inFormat=new SimpleDateFormat("hh:mm:ss");
	        DateFormat OutFormat=new SimpleDateFormat("HH:MM:SS");
	        
	        Date date=null;
	        try {
	            date=inFormat.parse(s);
	            
	        }
	        catch (ParseException e)
	        {
	            e.printStackTrace();
	        }
	        
	        if (date!=null) {
	            String myDate = OutFormat.format(date);
	            System.out.println(myDate);
	            
	        }
	        
	        sc.close();
	        
	        
	        
	        
	    }


}

