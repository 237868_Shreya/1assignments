package collection;

import java.util.ArrayList;
import java.util.Iterator;

public class JavaIteratorExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<Object> list = new ArrayList<>();
		  
		  list.add(777);
		  list.add("BTS");
		  list.add(555.5);
		  list.add("TXT");
		  
		 Iterator<Object> it = list.iterator();
		 while(it.hasNext()) {
		  System.out.println(it.next());
		 }
		 

	}

}
