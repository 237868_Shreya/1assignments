package staticdemo;

public class StaticMethod {
	
	void play() {
		System.out.println("I play guitar");
	}
	
	static void sleep() {
		System.out.println("I sleep at 1");
	}
	public static void main(String[] args) {
		sleep();
		StaticMethod sm = new StaticMethod();
		sm.play();

	}

}
